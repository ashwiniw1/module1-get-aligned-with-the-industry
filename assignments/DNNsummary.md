# **Deep Neural Network**

A deep neural network is a neural network which consists of more than one hidden layer.

* The first layer is called the Input Layer.
* The last layer is called the Output Layer.
* All layers in between are called Hidden Layers.

<img src="/images/DNN.png" width="900" height="450">

## Activation Function
Activation functions are mathematical equations that determine the output of a neural network.
It determine the output of a deep learning model, its accuracy, and also the computational efficiency of training a model—which can make or break a large scale neural network

<img src="/images/activationfunction-1.png" width="600" height="300">

**Sigmoid/Logistic**
* Smooth gradient, preventing “jumps” in output values.
* Output values bound between 0 and 1, normalizing the output of each neuron.
* **f(x) = 1/(1+e^-x)**

<img src="/images/sigmoidlogisticgraph.png" width="300" height="300">

**ReLU**
* Computationally efficient— allows the network to converge very quickly
* Non-linear— although it looks like a linear function, ReLU has a derivative function and allows for backpropagation
* **f(x)=max(0,x)**

<img src="/images/relu.png" width="300" height="300">

## Gradient Descent
* Gradient descent is used to minimize the function by iteratively moving in the direction of steepest descent ie it tells the direction in which you should step to increase the function much faster and to minimize the output deviation.

<img src="/images/gradientdescent.png" width="350" height="350">

## Backpropagation
* Backpropagation is an algorithm commonly used to train neural networks. 
* When the neural network is initialized, weights are set for its individual elements, called neurons. 