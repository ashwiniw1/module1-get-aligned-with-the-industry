# **Convolutionsl Neural Network**

A Convolutional Neural Network is basically applied on image data.It is a Deep Learning algorithm which can take in an input image, assign importance to various aspects/objects in the image ,analyze them and then be able to differentiate one from the other.

<img src="/images/CNN.jpeg" width="1000" height="400">

### Filter :
A filter in a CNN is like a weight matrix with which we multiply a part of the input image to generate a convoluted output. 

### Kernal size(k):
Kernel size refers to the width x height of the filter.

### Padding :
Padding refers to adding extra layer of zeros across the images so that the output image has the same size as the input. 

<img src="/images/padding.jpeg" width="200" height="200">

### Stride :
Stride is the number of pixels shifts over the input matrix.

### Pooling :
The pooling layers are mostly introduced in between the convolution layers to reduce a number of parameters and prevent over-fitting. 
The most common type of pooling is a pooling layer of filter size(2,2) using the MAX operation.

### Flattening :
It is converting the data into a 1 dimensional array for inputting it to the next layer.
